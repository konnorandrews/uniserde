/*
use std::any::TypeId;
use effectful::blocking::BlockingSpin;

use common::protocol::sequence::{MockSequenceScope, MockSequenceVisitor};
use treaty::{
    any::OwnedStatic,
    protocol::{
        visitor::{Sequence, VisitResult},
        DynVisitor,
    },
    Flow,
};

use crate::common::{builder::MockBuilder};

mod common;

#[test]
fn sequence_has_scope_with_size_hint_and_next() {
    let mut mock = MockSequenceVisitor::<BlockingSpin>::new();

    // Expect a visit with the sequence protocol.
    mock.expect_visit().once().returning(|scope| {
        // Get the size hint from the sequence scope.
        assert_eq!(scope.size_hint().into_value(), (1, Some(1)));

        let mut visitor = MockBuilder::<(), (), (), BlockingSpin>::new();

        // Expect the walker to lookup a trait.
        visitor.expect_traits().once().return_const(None);

        // Get the next item in the sequence from the walker.
        assert_eq!(
            scope.next(DynVisitor(&mut visitor)).into_value(),
            Flow::Done
        );

        // We are done.
        VisitResult::Control(Flow::Done)
    });

    // Everything goes throw the sequence protocol trait.
    let visitor: &mut dyn Sequence<BlockingSpin> = &mut mock;

    let mut scope = MockSequenceScope::<BlockingSpin>::new();

    // Expect a size hint to be asked for.
    scope.expect_size_hint().once().return_const((1, Some(1)));

    // Expect that one element is gotten from the sequence.
    scope.expect_next().once().returning(|visitor| {
        // Attempt to lookup a trait on the visitor.
        assert!(visitor
            .upcast::<ValueProto<OwnedStatic<i32>, BlockingSpin>>()
            .is_none());

        // We are done.
        Flow::Done
    });

    // Visit a sequence.
    assert!(matches!(
        visitor.visit(&mut scope).into_value(),
        VisitResult::Control(Flow::Done)
    ));
}

#[test]
fn sequence_proto() {
    // The type id of the higher ranked type.
    let id = TypeId::of::<SequenceProto<BlockingSpin>>();

    // The type id for the lifetime containing value protocol trait object.
    let name_id = TypeNameId::of_lower::<dyn Sequence<BlockingSpin>, BlockingSpin>();

    // They should be the same.
    assert_eq!(id, name_id.into_type_id());
}
*/
