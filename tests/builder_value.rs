mod common;

use common::walker::MockWalker;
use effectful::blocking::BlockingSpin;
use effectful::Effective;
use treaty::{
    any::{type_name, BorrowedStatic, OwnedStatic, TempBorrowedStatic},
    protocol::{
        visitor::{request_hint, Value},
        AsVisitor as _, DynWalker,
    },
    transform::BuildExt as _,
    Builder as _, Flow,
};

use crate::common::protocol::{hint::MockHintWalker, value::ValueVisitorExt as _};

#[test]
fn value_builder_gives_value_protocol_as_hint() {
    // Create a builder for a i32.
    let mut builder = i32::new_builder();

    // Expect the value builder to hint the value protocol with a owned i32.
    let mut walker = MockHintWalker::<
        type_name::Raised<'_, '_, dyn Value<'_, OwnedStatic<i32>, BlockingSpin>>,
    >::new();
    walker.expect_hint().once().returning(|mut visitor, ()| {
        // Fulfill the hint by visiting with a i32 value.
        assert_eq!(
            visitor.as_known().visit(OwnedStatic(42)).wait(),
            Flow::Done.into()
        );

        // We are done as the walker.
        Flow::Done.into()
    });

    // Request a hint from the i32 builder for what protocol to use.
    assert_eq!(
        request_hint::<BlockingSpin>(builder.as_visitor(), DynWalker::new(&mut walker)).wait(),
        Flow::Done.into()
    );

    // The builder should have the value.
    assert_eq!(builder.build().wait().unwrap(), 42);
    todo!()
}

#[test]
fn value_builder_can_use_an_owned_value_or_a_borrowed_value() {
    assert_eq!(
        i32::build({
            let mut walker = MockWalker::<(), (), BlockingSpin>::new();
            walker.expect_walk().once().returning(|mut visitor| {
                visitor.visit_value_and_done(OwnedStatic(1));
                Ok(())
            });
            walker
        })
        .unwrap(),
        1
    );

    assert_eq!(
        i32::build({
            let mut walker = MockWalker::<(), (), BlockingSpin>::new();
            walker.expect_walk().once().returning(|mut visitor| {
                visitor.visit_value_and_done(BorrowedStatic(&2));
                Ok(())
            });
            walker
        })
        .unwrap(),
        2
    );

    assert_eq!(
        i32::build({
            let mut walker = MockWalker::<(), (), BlockingSpin>::new();
            walker.expect_walk().once().returning(|mut visitor| {
                visitor.visit_value_and_done(TempBorrowedStatic(&3));
                Ok(())
            });
            walker
        })
        .unwrap(),
        3
    );
}
