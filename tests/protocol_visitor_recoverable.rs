/*
use common::protocol::recoverable::MockRecoverableVisitor;
use effectful::blocking::BlockingSpin;
use treaty::{
    any::OwnedStatic,
    protocol::{
        visitor::{Recoverable, VisitResult},
        DynVisitor,
    },
    Flow, Status,
};

use crate::common::{
    builder::MockBuilder, protocol::recoverable::MockRecoverableScopeVisitor,
};

mod common;

/// Tests that the recoverable protocol allows multiple walks by the visitor.
#[test]
fn recoverable_can_be_visited() {
    let mut mock = MockRecoverableVisitor::<BlockingSpin>::new();

    // Expect a visit using the rescoverable protocol.
    mock.expect_visit().once().returning(|scope| {
        let mut visitor = MockBuilder::<(), (), (), BlockingSpin>::new();

        // Expect that the visitor gets used.
        visitor.expect_traits().times(2).return_const(None);

        // Attempt to walk once.
        assert_eq!(
            scope.new_walk(DynVisitor(&mut visitor)).into_value(),
            Status::Ok
        );

        // Attempt to walk twice.
        assert_eq!(
            scope.new_walk(DynVisitor(&mut visitor)).into_value(),
            Status::Ok
        );

        // We are done.
        VisitResult::Control(Flow::Done)
    });

    let visitor: &mut dyn Recoverable<BlockingSpin> = &mut mock;

    let mut scope = MockRecoverableScopeVisitor::new();

    // Expect two walks of the recoverable walker.
    scope.expect_new_walk().times(2).returning(|visitor| {
        // Attempt to use the visitor.
        assert!(visitor
            .upcast::<ValueProto<OwnedStatic<i32>, BlockingSpin>>()
            .is_none());

        Status::Ok
    });

    // Visit using the recoverable protocol.
    assert!(matches!(
        visitor.visit(&mut scope).into_value(),
        VisitResult::Control(Flow::Done)
    ));
}
*/
