use effectful::{
    bound::{Bool, IsSend, IsSync},
    effective::{Canonical, Effective},
    environment::Environment,
    forward_send_sync,
};
use mockall::mock;
use treaty::{
    any::{type_name, AnyTrait},
    protocol::DynWalker,
    protocol::{
        visitor::{DynSequenceScope, RequestHint, Sequence, SequenceScope, Value, VisitResult},
        DynVisitor,
    },
    Flow,
};

mock! {
    pub SequenceVisitor<E> {
        pub fn visit<'a, 'ctx>(&mut self, scope: DynSequenceScope<'a, 'ctx, E>) -> VisitResult;
    }
}

forward_send_sync!({} {} {E: (Environment + Send)} MockSequenceVisitor<E>);

// any_trait! {
//     impl['ctx][E] MockSequenceVisitor<E> = [
//         SequenceProto<E>
//     ] where
//         E: Environment,
// }

impl<'ctx, E: Environment + Send> Sequence<'ctx, E> for MockSequenceVisitor<E> {
    fn visit<'r>(
        &'r mut self,
        scope: DynSequenceScope<'r, 'ctx, E>,
    ) -> Canonical<'r, VisitResult, E> {
        E::value(self.visit(scope)).cast()
    }
}

mock! {
    pub SequenceScope<E: Environment> {
        pub fn size_hint(&mut self) -> (usize, Option<usize>);
        pub fn next<'r, 'ctx>(&'r mut self, visitor: DynVisitor<'r, 'ctx, E>) -> Flow;
    }
}

forward_send_sync!({} {} {E: (Environment + Send)} MockSequenceScope<E>);

impl<'ctx, E: Environment + Send> SequenceScope<'ctx, E> for MockSequenceScope<E> {
    fn size_hint(&mut self) -> Canonical<'_, (usize, Option<usize>), E> {
        E::value(self.size_hint()).cast()
    }

    fn next<'r>(&'r mut self, visitor: DynVisitor<'r, 'ctx, E>) -> Canonical<'r, Flow, E> {
        E::value(self.next(visitor)).cast()
    }
}
