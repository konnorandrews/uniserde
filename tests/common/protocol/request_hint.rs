use effectful::{
    bound::{Bool, IsSend, IsSync},
    effective::{Canonical, Effective},
    environment::Environment,
    forward_send_sync,
};
use mockall::mock;
use treaty::{
    any::{type_name, AnyTrait},
    protocol::visitor::{RequestHint, Value, VisitResult},
    protocol::DynWalker,
    Flow,
};

mock! {
    pub RequestHintVisitor<E: Environment> {
        pub fn request_hint<'r, 'src>(&'r mut self, walker: DynWalker<'r, 'src, E>) -> VisitResult;
    }
}

forward_send_sync!({} {} {E: (Environment + Send)} MockRequestHintVisitor<E>);

// any_trait! {
//     impl['ctx][E] MockRequestHintVisitor<E> = [
//         RequestHintProto<E>
//     ] where
//         E: Environment,
// }

impl<'ctx, E: Environment + Send> RequestHint<'ctx, E> for MockRequestHintVisitor<E> {
    fn request_hint<'r>(
        &'r mut self,
        walker: DynWalker<'r, 'ctx, E>,
    ) -> Canonical<'r, VisitResult, E> {
        E::value(self.request_hint(walker)).cast()
    }
}
