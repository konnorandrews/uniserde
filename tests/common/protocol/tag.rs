use effectful::{
    blocking::BlockingSpin,
    bound::{Bool, IsSend, IsSync},
    effective::{Canonical, Effective},
    environment::Environment,
    forward_send_sync,
};
use mockall::mock;
use treaty::{
    any::AnyTrait,
    protocol::{
        visitor::{visit_tag, ConstTagKind, Tag, TagConst, TagKind, VisitResult},
        AsVisitor, DynVisitor,
    },
    walk::DynWalkerObjSafe,
    Flow, Walker,
};

mock! {
    pub TagVisitor<K: TagKind<E>, E: Environment> {
        pub fn visit<'r, 'ctx>(&'r mut self, kind: K, walker: DynWalkerObjSafe<'r, 'ctx, E>) -> VisitResult;
    }
}

forward_send_sync!({K: (TagKind<E> + Send)} {} {E: (Environment + Send)} MockTagVisitor<K, E>);

// any_trait! {
//     impl['ctx, K][E] MockTagVisitor<K, E> = [
//         TagProto<K, E>,
//     ] where
//         K: TagKind<E>,
//         E: Environment,
// }

impl<'ctx, K: TagKind<E> + Send, E: Environment + Send> Tag<'ctx, K, E> for MockTagVisitor<K, E> {
    fn visit<'r>(
        &'r mut self,
        kind: K,
        walker: DynWalkerObjSafe<'r, 'ctx, E>,
    ) -> Canonical<'r, VisitResult, E> {
        E::value(self.visit(kind, walker)).cast()
    }
}

pub trait TagVisitorExt<'ctx> {
    fn visit_tag_and_done<'a, T: ConstTagKind<BlockingSpin>, W: Walker<'ctx, BlockingSpin>>(
        &'a mut self,
        walker: W,
    );
}

impl<'ctx, T> TagVisitorExt<'ctx> for T
where
    T: AsVisitor<'ctx, BlockingSpin>,
{
    fn visit_tag_and_done<'a, Tag: ConstTagKind<BlockingSpin>, W: Walker<'ctx, BlockingSpin>>(
        &'a mut self,
        walker: W,
    ) {
        let result = visit_tag::<Tag, BlockingSpin, _>(Tag::NEW, self.as_visitor(), walker).wait();

        assert_eq!(result.unwrap(), VisitResult::Control(Flow::Done));
    }
}
