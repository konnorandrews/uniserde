use effectful::{
    bound::{Bool, IsSend, IsSync},
    effective::{Canonical, Effective},
    environment::Environment,
    forward_send_sync, DynBind,
};
use mockall::mock;
use treaty::{any::AnyTrait, build::BuilderTypes, protocol::DynVisitor, Builder, Walker};

mock! {
    pub Walker<Output, Error, E: Environment> {
        pub fn walk<'r, 'ctx>(self, visitor: DynVisitor<'r, 'ctx, E>) -> Result<Output, Error>;

        // pub fn traits(&self, id: TypeNameId) -> &Option<Box<dyn for<'ctx> AnyTrait<'ctx, E>>>;
        //
        // pub fn traits_mut(&mut self, id: TypeNameId) -> &mut Option<Box<dyn for<'ctx> AnyTrait<'ctx, E>>>;
    }
}

forward_send_sync!({Output, Error} {} {E: (Environment + Send)} MockWalker<Output, Error, E>);

impl<'ctx, Output: DynBind<E>, Error: DynBind<E> + core::fmt::Debug, E: Environment + Send>
    Walker<'ctx, E> for MockWalker<Output, Error, E>
{
    type Error = Error;

    type Output = Output;

    fn walk<'r>(
        self,
        visitor: DynVisitor<'r, 'ctx, E>,
    ) -> Canonical<'r, Result<Self::Output, Self::Error>, E>
    where
        Self: 'r,
    {
        E::value(self.walk(visitor)).cast()
    }
}

impl<'ctx, Output: DynBind<E>, Error: DynBind<E>, E: Environment> AnyTrait<'ctx>
    for MockWalker<Output, Error, E>
{
    // fn upcast_to_id<'a>(
    //     &'a self,
    //     id: TypeNameId,
    // ) -> Option<AnyTraitObject<'a, 'ctx, indirect::Ref, E>>
    // where
    //     'ctx: 'a,
    // {
    //     self.traits(id).as_ref().and_then(|t| t.upcast_to_id(id))
    // }
    //
    // fn upcast_to_id_mut<'a>(
    //     &'a mut self,
    //     id: TypeNameId,
    // ) -> Option<AnyTraitObject<'a, 'ctx, indirect::Mut, E>>
    // where
    //     'ctx: 'a,
    // {
    //     self.traits_mut(id)
    //         .as_mut()
    //         .and_then(|t| t.upcast_to_id_mut(id))
    // }
    //
    // type Available = () where Self: Sized;
}
