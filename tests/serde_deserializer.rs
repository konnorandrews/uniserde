/*
use serde_json::json;
use treaty::walk::walkers::serde::deserializer::DeserializerWalker;
use treaty::{transform::BuildExt as _, Build};

use effectful::SendSync;
use macro_rules_attribute::derive;

mod common;

#[test]
fn demo() {
    let x = json!(true);

    let y = bool::build(DeserializerWalker::new(x));

    assert!(y.unwrap());
}

#[test]
fn demo2() {
    let mut de = serde_json::Deserializer::from_str("42");
    let y = u8::build(DeserializerWalker::new(&mut de));

    assert_eq!(y.unwrap(), 42);
}

#[derive(Build!, Debug, PartialEq)]
struct X {
    a: bool,
    b: i64,
    c: Y,
}

#[derive(Build!, Debug, PartialEq)]
struct Y {
    name: String,
    age: u8,
}

#[test]
fn demo3() {
    let x = json!({ "a": true, "b": 42, "c": { "name": "hi", "age": 200 }});

    let y = X::build(DeserializerWalker::new(x));

    assert_eq!(
        y.unwrap(),
        X {
            a: true,
            b: 42,
            c: Y {
                name: "hi".into(),
                age: 200
            }
        }
    );
}
*/
