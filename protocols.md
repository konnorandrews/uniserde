# Walker

## General Layout
All the standard flows use the following structure.

- Request Hint: Allow the builder to get exactly what it wants instead of using the full flow.
- Header Tags: Tags for meta information about the value. These are what provide the rich structure of data.
- Sequence / Value: The two basic ways to pass data are by a sequence of walkers or a single value.

Nothing stops a walker from emitting multiple values to form a sequence. However, most builders
will not expect this behavior. Instead, the sequence protocol allows a builder to construct a sequence
of values *it* wants instead of what the walker is generating.

## Map
A sequence of key tagged values

A map emits everything a plain sequence would.
This allows using maps with builders that only undertand sequences.
Note, the keys of the values will be lost if the builder doesn't understand key tags.

- Tag - Map: A empty value that signals the value is a map instead of a normal sequence.
- Sequence: A sequence of the key tagged values.
  - Tag - Key: A (hopefully unique) key for the value.
  - <flatten>: The value.

## Struct
A struct in Rust and more generally as a map.

A struct emits everything a map would, and also it emits everything a plain sequence would.
This allows using structs with builders that only understand maps and/or sequences.

- Request Hint: Allow the builder to directly ask for what it wants.
- Value: Attempt to give the struct directly to the builder.
- Tag - Type ID: Useful for looking up things in a type map. Only visited if the struct is a Rust type.
- Tag - Struct: A empty value that signals the value is a struct instead of just a map.
  - Tag - Map: This only happens if the visitor didn't recognize the struct tag.
- Tag - Type Name: The name of the type.
- Tag - Field Names: All the field names of the struct.
  - Sequence: A sequence of strings.
    - Value: Field name as a `&'static str`.
- Sequence: A sequence of the fields.
  - Tag - Field: A empty value that signals the value is a struct field instead of just a map key.
  - Tag - Key: The field name.
    - Value: Field name as a `&'static str`.
  - <flatten>: The value.
