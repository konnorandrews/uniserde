# Seeds for failure cases proptest has generated in the past. It is
# automatically read and these particular cases re-run before any
# novel cases are generated.
#
# It is recommended to check this file in to source control so that
# everyone who runs the test benefits from these saved cases.
cc 1725c26548f1c5297668210ab4dda12dc45754627a4019359763c1c9fd6913a3 # shrinks to x = 4
cc 3c8175ce49a080bb929b2781b090073cf0700415a08db6b7c39f32b59592661b # shrinks to s = "XA0A0a_"
cc 3569a9f258aeb7d1dd27d9261dd0d511b4394c0fd2d4a386d8445d9c98756f30 # shrinks to str = " -DA5D_"
cc 779296ede6c0180afb57a35b1c128ade1435d02a6db2ed68e18efb5c7785ef2f # shrinks to str = "0E-0- "
cc 77c97f17e9195785d45fe2a040ccdb4932501ab8b1855ba19870f43454a337b4 # shrinks to str = "708QE"
cc dcf95b6e6c0f189109f4e22b84831c4881dd0ea10c6a87da3ca8ec984de427fa # shrinks to str = ""
cc 7f0b586af42923f396d792161f736dc475e8eb1afd1ab991a13fdc6b455b54ae # shrinks to str = "0A-A 00"
cc e4c41856713df30321a99e3e647f5541db9789f13cf2f2416380a0931be4a677 # shrinks to str = "-Xj0Ab"
