use effectful::{
    asynchronous::Async,
    blocking::Blocking,
    bound::{HasSend, IsSend, No},
    thunk::{ThunkMaybeSendOps as _, ThunkOps as _},
};
use treaty::{
    self,
    build::{builders::core::value::ValueBuilder, Builder as _},
    protocol::AsVisitor as _,
    walk::{walkers::core::value::ValueWalker, Walker},
};
use ty_tag::tag;

#[derive(Debug)]
struct Demo(i32);

#[tag]
type DemoTag = Demo;

fn main() {
    let x = String::from("demo");
    let walker = ValueWalker::new(&*x);

    let mut builder = ValueBuilder::<&str, Blocking>::from_seed(())
        .finish_maybe_send()
        .into_inner();

    walker
        .walk(builder.as_visitor())
        .finish_maybe_send()
        .into_inner()
        .unwrap();

    let HasSend(x) = builder
        .build()
        .into_inner()
        .finish_maybe_send()
        .into_inner()
        .unwrap();

    dbg!(x);
}
