use core::convert::Infallible;

use effectful::{
    bound::{Bool, HasSend, IsSend},
    environment::{Environment, NeedSendOf, ThunkMaybeSend},
    thunk::ThunkMaybeSendOps as _,
};
use ty_tag::{lifetime_list::L1, modifiers::AddLt, ReifySelf};

use crate::{
    protocol::{
        visitor::{visit_value, ValueSrcSend},
        DynVisitor,
    },
    walk::Walker,
};

/// A very basic walker that uses the [`Value`][crate::protocol::visitor::value::Value] protocol.
///
/// Primitive types use this walker as their main walker.
/// This walker doesn't consider it an error if the visitor doesn't have the protocol.
#[derive(Debug)]
pub struct ValueWalker<T>(T);

#[allow(unsafe_code)]
unsafe impl<T: Send, F: Bool> IsSend<F> for ValueWalker<T> {}

impl<T> ValueWalker<T> {
    /// Create walker from a value.
    #[inline(always)]
    pub fn new(value: T) -> Self {
        Self(value)
    }
}

impl<T> From<T> for ValueWalker<T> {
    #[inline(always)]
    fn from(value: T) -> Self {
        Self::new(value)
    }
}

impl<T: Copy> From<&T> for ValueWalker<T> {
    #[inline(always)]
    fn from(value: &T) -> Self {
        Self::new(*value)
    }
}

impl<'ctx, T, E> Walker<'ctx, E> for ValueWalker<T>
where
    E: Environment,
    T: Send + ReifySelf<L1<'ctx>>,
{
    type Error = Infallible;

    type Output = ();

    fn walk<'r>(
        self,
        visitor: DynVisitor<'r, 'ctx, E>,
    ) -> ThunkMaybeSend<'r, E, Result<Self::Output, Self::Error>>
    where
        Self: 'r,
    {
        // Attempt to visit using the value protocol.
        visit_value::<ValueSrcSend<AddLt<T::Tag>>, E>(visitor, HasSend(self.0))
            .map_maybe_send(|_| Ok(()))
    }
}

// /// Borrowed form of [`ValueWalker`].
// ///
// /// This walker supports values borrowed for `'ctx` or longer.
// #[derive(SendSync)]
// pub struct BorrowWalker<'ctx, T: ?Sized>(Dynamic<&'ctx T>);
//
// impl<'ctx, T: ?Sized> BorrowWalker<'ctx, T> {
//     /// Create walker from a value.
//     #[inline(always)]
//     pub fn new(value: &'ctx T) -> Self {
//         Self(Dynamic(value))
//     }
// }
//
// impl<'ctx, T: ?Sized + 'static, E: Environment> crate::Walker<'ctx, E> for BorrowWalker<'ctx, T>
// where
//     Dynamic<&'ctx T>: DynBind<E>,
//     Dynamic<BorrowedStatic<'ctx, T>>: DynBind<E>,
//     for<'a> Dynamic<TempBorrowedStatic<'a, T>>: DynBind<E>,
// {
//     type Error = Never;
//
//     type Output = Dynamic<&'ctx T>;
//
//     #[inline(always)]
//     fn walk<'b: 'c, 'd: 'c, 'c>(
//         self,
//         visitor: DynVisitor<'b, 'd, 'ctx, E>,
//     ) -> Canonical<'c, Result<Self::Output, Self::Error>, E> {
//         // Attempt to visit using the value protocol.
//         E::value((self, visitor))
//             .update_map((), |_, (this, visitor)| {
//                 visit_value::<_, E>(visitor.cast(), BorrowedStatic(this.0 .0))
//                     .map((), |_, x| VisitResult::unit_skipped(x))
//                     .cast()
//             })
//             .if_skipped((), |_, (this, visitor)| {
//                 visit_value::<_, E>(visitor.cast(), TempBorrowedStatic(this.0 .0))
//                     .map((), |_, x| VisitResult::unit_skipped(x))
//                     .cast()
//             })
//             .map((), |_, ((this, _), _)| Ok(this.0))
//             .cast()
//     }
// }
