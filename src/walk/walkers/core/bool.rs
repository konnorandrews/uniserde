use effectful::{
    bound::Dynamic,
    effective::{Canonical, Effective},
    environment::Environment, DynBind,
};

use crate::{any::OwnedStatic, Walk};

use super::value::ValueWalker;

impl<'ctx, M, E: Environment> Walk<'ctx, M, E> for bool
where
    Dynamic<bool>: DynBind<E>,
    Dynamic<OwnedStatic<bool>>: DynBind<E>,
{
    type Walker = ValueWalker<bool>;

    fn into_walker<'e>(self) -> Canonical<'e, Self::Walker, E> {
        E::value(ValueWalker::new(self)).cast()
    }
}

impl<'ctx, M, E: Environment> Walk<'ctx, M, E> for &'ctx bool
where
    Dynamic<bool>: DynBind<E>,
    Dynamic<OwnedStatic<bool>>: DynBind<E>,
{
    type Walker = ValueWalker<bool>;

    fn into_walker<'e>(self) -> Canonical<'e, Self::Walker, E> {
        E::value(ValueWalker::new(*self)).cast()
    }
}
