use core::marker::PhantomData;

use effectful::{
    bound::IsSync,
    effective::{Canonical, Effective},
    environment::Environment,
    SendSync,
};

use crate::{
    any::AnyTrait,
    protocol::{
        visitor::{SequenceScope, TagError},
        DynVisitor,
    },
    Flow, Never,
};

#[derive(SendSync)]
pub struct StaticSliceWalker<T: 'static, W> {
    names: &'static [T],
    current: usize,
    _marker: PhantomData<fn() -> W>,
}

impl<T, W> StaticSliceWalker<T, W> {
    pub fn new(names: &'static [T]) -> Self {
        Self {
            names,
            current: 0,
            _marker: PhantomData,
        }
    }
}

impl<'ctx, T, W, E> crate::Walker<'ctx, E> for StaticSliceWalker<T, W>
where
    E: Environment,
    W: crate::Walker<'ctx, E, Output = ()>,
    T: IsSync<E::NeedSend> + IsSync<E::NeedSync>,
    &'static T: Into<W>,
{
    type Error = TagError<Never>;

    type Output = ();

    #[inline(always)]
    fn walk<'b: 'c, 'd: 'c, 'c>(
        self,
        _visitor: DynVisitor<'b, 'd, 'ctx, E>,
    ) -> Canonical<'c, Result<Self::Output, Self::Error>, E> {
        todo!()
        // E::wrap(async move {
        //     match visit_request_hint::<E>(visitor.cast(), DynWalker(&mut self)).await {
        //         VisitResult::Skipped(_) | VisitResult::Control(Flow::Continue) => {}
        //         _ => return Ok(()),
        //     }
        //
        //     match visit_value::<_, E>(visitor.cast(), OwnedStatic(self.names)).await {
        //         VisitResult::Skipped(_) => {}
        //         VisitResult::Control(Flow::Continue) => {}
        //         _ => return Ok(()),
        //     }
        //
        //     if let Some(object) = visitor.cast().upcast_mut::<SequenceProto<E>>() {
        //         // Visit with the name. Ignore the flow because we return a result not a flow.
        //         let _ = object.visit(&mut self).await;
        //     }
        //
        //     Ok(())
        // })
    }
}

impl<'lt, 'ctx: 'lt, T, W: 'lt> AnyTrait<'lt, 'ctx> for StaticSliceWalker<T, W> {}

// any_trait! {
//     impl['a, 'ctx, T, W][E] StaticSliceWalker<T, W> = [
//     ] where E: Environment, T: IsSync<E::NeedSend> + IsSync<E::NeedSync>
// }

impl<'ctx, T, W, E> SequenceScope<'ctx, E> for StaticSliceWalker<T, W>
where
    E: Environment,
    W: crate::Walker<'ctx, E, Output = ()>,
    T: IsSync<E::NeedSend> + IsSync<E::NeedSync>,
    &'static T: Into<W>,
{
    #[inline(always)]
    fn next<'a: 'c,  'd: 'c, 'b: 'c, 'c>(
        &'a mut self,
        _visitor: DynVisitor<'b, 'd, 'ctx, E>,
    ) -> Canonical<'c, Flow, E> {
        if let Some(_name) = self.names.get(self.current) {
            self.current += 1;
            todo!()
            // E::wrap(async move {
            //     match crate::Walker::walk(name.into(), visitor).await {
            //         Ok(()) => Flow::Continue,
            //         Err(_) => unreachable!(),
            //     }
            // })
        } else {
            E::value(Flow::Done).cast()
        }
    }

    #[inline(always)]
    fn size_hint(&mut self) -> Canonical<'_, (usize, Option<usize>), E> {
        E::value((self.names.len(), Some(self.names.len()))).cast()
    }
}
