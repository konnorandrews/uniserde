use effectful::{
    effective::{Canonical, Effective},
    environment::Environment,
    SendSync,
};

use crate::{protocol::DynVisitor, Never};

/// A walker that does nothing.
///
/// This walker is useful for tags that don't need a value.
#[non_exhaustive]
#[derive(Debug, Default, SendSync)]
pub struct NoopWalker;

impl NoopWalker {
    pub fn new() -> Self {
        Self
    }
}

impl<'ctx, E: Environment> crate::Walker<'ctx, E> for NoopWalker {
    type Error = Never;

    type Output = ();

    fn walk<'b: 'c, 'd: 'c, 'c>(
        self,
        _visitor: DynVisitor<'b, 'd, 'ctx, E>,
    ) -> Canonical<'c, Result<Self::Output, Self::Error>, E> {
        E::value(Ok(())).cast()
    }
}
