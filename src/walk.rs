pub mod walkers;

use core::fmt::Debug;

use effectful::{
    bound::{HasSend, IsSend},
    environment::{Environment, NeedSendOf, Thunk, ThunkMaybeSend},
    thunk::{
        Ctx, CtxMut, IntoThunk as _, IntoThunkMaybeSend as _, ThunkMaybeSendOps as _, ThunkOps as _,
    },
};

use crate::{protocol::DynVisitor, Flow};

/// A type that can be walked.
pub trait Walk<'src, M, E: Environment>: Sized {
    /// The walker for the type.
    type Walker: Walker<'src, E>;

    #[must_use]
    fn into_walker<'u>(self) -> ThunkMaybeSend<'u, E, Self::Walker>
    where
        Self: 'u;
}

/// Walker for a type.
///
/// The `'ctx` lifetime is some lifetime that is longer than `Self`.
/// Data from the value may borrow using `'ctx`.
///
/// The way to use a walker is as follows.
/// - Call [From::from()] with a value to be walked to make a walker.
/// - Call [Self::walk()] to walk the value. Data will be sent to the provided
///     visitor.
pub trait Walker<'src, E: Environment>: IsSend<NeedSendOf<E>> {
    type Error: IsSend<NeedSendOf<E>> + Debug;

    /// An arbitrary type the walker is left with after walking.
    ///
    /// Its recommended that this is `Self` if the walker is repeatable.
    type Output: IsSend<NeedSendOf<E>>;

    /// Walk the value.
    ///
    /// The walker should send data to the `visitor` as it walks the value.
    fn walk<'r>(
        self,
        visitor: DynVisitor<'r, 'src, E>,
    ) -> ThunkMaybeSend<'r, E, Result<Self::Output, Self::Error>>
    where
        Self: 'r;
}

pub trait WalkerObjSafe<'src, E: Environment>: IsSend<NeedSendOf<E>> {
    fn walk<'r>(&'r mut self, visitor: DynVisitor<'r, 'src, E>) -> Thunk<'r, E, Flow>
    where
        'src: 'r;
}

pub type DynWalkerObjSafe<'r, 'src, E> = &'r mut dyn WalkerObjSafe<'src, E>;

enum DynWalkerState<'src, W: Walker<'src, E>, E: Environment> {
    Walking,
    Pending(W),
    Done(W::Output),
    Err(W::Error),
}

pub enum DynWalkerError<'src, W: Walker<'src, E>, E: Environment> {
    NeverWalked(W),

    /// This can only happen if a panic happens furing the walk and is then caught before calling
    /// finish..
    WalkNeverFinished,

    Walker(W::Error),

    WasWalked(W::Output),
}

pub struct DynWalkerAdapter<'src, W: Walker<'src, E>, E: Environment> {
    state: DynWalkerState<'src, W, E>,
}

impl<'src, W: Walker<'src, E>, E: Environment> DynWalkerAdapter<'src, W, E> {
    #[inline(always)]
    pub fn new(walker: W) -> Self {
        Self {
            state: DynWalkerState::Pending(walker),
        }
    }

    #[inline(always)]
    pub fn finish(self) -> Result<W::Output, DynWalkerError<'src, W, E>> {
        match self.state {
            DynWalkerState::Walking => Err(DynWalkerError::WalkNeverFinished),
            DynWalkerState::Pending(walker) => Err(DynWalkerError::NeverWalked(walker)),
            DynWalkerState::Done(value) => Ok(value),
            DynWalkerState::Err(err) => Err(DynWalkerError::Walker(err)),
        }
    }

    #[inline(always)]
    pub fn into_inner(self) -> Result<W, DynWalkerError<'src, W, E>> {
        match self.state {
            DynWalkerState::Walking => Err(DynWalkerError::WalkNeverFinished),
            DynWalkerState::Pending(walker) => Ok(walker),
            DynWalkerState::Done(value) => Err(DynWalkerError::WasWalked(value)),
            DynWalkerState::Err(err) => Err(DynWalkerError::Walker(err)),
        }
    }
}

impl<'src, W: Walker<'src, E>, E: Environment> WalkerObjSafe<'src, E>
    for DynWalkerAdapter<'src, W, E>
where
    Self: IsSend<NeedSendOf<E>>,
{
    #[inline(always)]
    fn walk<'r>(&'r mut self, visitor: DynVisitor<'r, 'src, E>) -> Thunk<'r, E, Flow>
    where
        'src: 'r,
    {
        if let DynWalkerState::Pending(walker) =
            core::mem::replace(&mut self.state, DynWalkerState::Walking)
        {
            Ctx::new((self, visitor), walker)
                .into_thunk_maybe_send()
                .update_maybe_send(
                    |CtxMut {
                         state: (this, visitor),
                         output: walker,
                     }| {
                        // Walk the walker.
                        walker.walk(visitor.cast()).map_maybe_send_capture(
                            this,
                            |this, CtxMut { output: value, .. }| {
                                match value {
                                    Ok(value) => {
                                        this.state = DynWalkerState::Done(value);
                                        HasSend(Flow::Done)
                                    }
                                    Err(err) => {
                                        this.state = DynWalkerState::Err(err);

                                        // Signal that control flow should stop as soon as possible as we
                                        // are in an error state.
                                        HasSend(Flow::Err)
                                    }
                                }
                            },
                        )
                    },
                )
                .no_state_maybe_send()
                .into_thunk()
                .map(|ctx| ctx.output.0)
        } else {
            // Can't do anything if the walker has already been walked.
            Ctx::output(Flow::Done).into_thunk()
        }
    }
}
