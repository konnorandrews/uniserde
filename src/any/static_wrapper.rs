//! Wrapper types that impl [`TypeName`] when their generic type `T` is `'static`.

use effectful::SendSync;

use crate::hkt::Marker;

use super::*;

/// Named `T` where `T: 'static`.
#[derive(PartialEq, Clone, Copy, Debug, SendSync)]
#[repr(transparent)]
pub struct OwnedStatic<T: ?Sized>(pub T);

impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Lower<'lt, 'ctx, &'lt &'ctx ()> for OwnedStatic<T> {
    type Lowered = OwnedStatic<T>;
}

impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Raise<'lt, 'ctx, &'lt &'ctx ()> for OwnedStatic<T> {
    type Raised = OwnedStatic<T>;
}

/// Named `&'ctx T` where` T: 'static`.
#[derive(PartialEq, Clone, Copy, Debug, SendSync)]
#[repr(transparent)]
pub struct BorrowedStatic<'ctx, T: ?Sized>(pub &'ctx T);

const _: () = {
    pub struct BorrowedStaticHrt<T: ?Sized>(Marker<T>);

    impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Lower<'lt, 'ctx, &'lt &'ctx ()>
        for BorrowedStaticHrt<T>
    {
        type Lowered = BorrowedStatic<'ctx, T>;
    }

    impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Raise<'lt, 'ctx, &'lt &'ctx ()>
        for BorrowedStatic<'ctx, T>
    {
        type Raised = BorrowedStaticHrt<T>;
    }
};

/// Named `&'lt T` where` T: 'static`.
#[derive(PartialEq, Clone, Copy, Debug, SendSync)]
#[repr(transparent)]
pub struct TempBorrowedStatic<'lt, T: ?Sized>(pub &'lt T);

const _: () = {
    pub struct TempBorrowedStaticHrt<T: ?Sized>(Marker<T>);

    impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Lower<'lt, 'ctx, &'lt &'ctx ()>
        for TempBorrowedStaticHrt<T>
    {
        type Lowered = TempBorrowedStatic<'lt, T>;
    }

    impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Raise<'lt, 'ctx, &'lt &'ctx ()>
        for TempBorrowedStatic<'lt, T>
    {
        type Raised = TempBorrowedStaticHrt<T>;
    }
};

/// Named `&'ctx mut T` where` T: 'static`.
#[derive(PartialEq, Debug, SendSync)]
#[repr(transparent)]
pub struct BorrowedMutStatic<'ctx, T: ?Sized>(pub &'ctx mut T);

const _: () = {
    pub struct BorrowedMutStaticHrt<T: ?Sized>(Marker<T>);

    impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Lower<'lt, 'ctx, &'lt &'ctx ()>
        for BorrowedMutStaticHrt<T>
    {
        type Lowered = BorrowedMutStatic<'ctx, T>;
    }

    impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Raise<'lt, 'ctx, &'lt &'ctx ()>
        for BorrowedMutStatic<'ctx, T>
    {
        type Raised = BorrowedMutStaticHrt<T>;
    }
};

/// Named `&'lt mut T` where` T: 'static`.
#[derive(PartialEq, Debug, SendSync)]
#[repr(transparent)]
pub struct TempBorrowedMutStatic<'lt, T: ?Sized>(pub &'lt mut T);

const _: () = {
    pub struct TempBorrowedMutStaticHrt<T: ?Sized>(Marker<T>);

    impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Lower<'lt, 'ctx, &'lt &'ctx ()>
        for TempBorrowedMutStaticHrt<T>
    {
        type Lowered = TempBorrowedMutStatic<'lt, T>;
    }

    impl<'lt, 'ctx, T: ?Sized + 'static> type_name::Raise<'lt, 'ctx, &'lt &'ctx ()>
        for TempBorrowedMutStatic<'lt, T>
    {
        type Raised = TempBorrowedMutStaticHrt<T>;
    }
};
