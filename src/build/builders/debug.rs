use core::{any::TypeId, marker::PhantomData};

use crate::{
    any::OwnedStatic,
    any_trait,
    effect::{Effect, Future},
    protocol::{
        self,
        visitor::{
            DynSequenceScope, RequestHint, RequestHintProto, Sequence, Tag, TagDyn, TagProto,
            Value, ValueProto, VisitResult,
        },
        DynVisitor,
    },
    protocol::{visitor::SequenceProto, DynWalker},
    DynWalkerObjSafe, Flow,
};

pub struct Visitor<E>(usize, PhantomData<fn() -> E>);

any_trait! {
    impl['ctx, E] Visitor<E> = [
        RequestHintProto<E>,
        // DynRecoverable<'a, 'ctx, E>,
        TagProto<TagDyn, E>,
        ValueProto<OwnedStatic<&'static str>, E>,
        ValueProto<OwnedStatic<TypeId>, E>,
        ValueProto<OwnedStatic<usize>, E>,
        ValueProto<OwnedStatic<bool>, E>,
        // DynValue<'a, 'ctx, OwnedStatic<&'static [&'static str]>, E>,
        SequenceProto<E>,
    ] else ref {
        let (_this, id);
        println!("Unknown trait: {:?}", id);
        None
    } else mut {
        let (_this, id);
        println!("Unknown trait: {:?}", id);
        None
    } where E: Effect
}

impl<E: Effect> Default for Visitor<E> {
    fn default() -> Self {
        Self::new()
    }
}

impl<E: Effect> Visitor<E> {
    pub fn new() -> Self {
        Self(0, PhantomData)
    }

    fn tab(&self) {
        if self.0 > 0 {
            for _ in 0..self.0 - 1 {
                print!(" | ");
            }
            print!(" |-");
        }
    }
}

impl<'ctx, E: Effect> RequestHint<'ctx, E> for Visitor<E> {
    fn request_hint<'a>(
        &'a mut self,
        _walker: DynWalker<'a, 'ctx>,
    ) -> Future<'a, VisitResult<DynWalker<'a, 'ctx>>, E> {
        // self.tab();
        // println!("Visit request hint (no hint given)");
        // println!("Visit request hint (hint for recoverable)");
        E::ready(Flow::Continue.into())
    }
}

impl<'ctx, E: Effect> Tag<'ctx, TagDyn, E> for Visitor<E> {
    fn visit<'a>(
        &'a mut self,
        kind: TagDyn,
        walker: DynWalkerObjSafe<'a, 'ctx, E>,
    ) -> Future<'a, VisitResult<DynWalkerObjSafe<'a, 'ctx, E>>, E> {
        E::wrap(async move {
            match kind.0 {
                crate::TAG_TYPE_NAME => {
                    self.tab();
                    println!("type name:");

                    self.0 += 1;
                    let _ = walker.walk(DynVisitor(self)).await;
                    self.0 -= 1;

                    Flow::Continue.into()
                }
                crate::TAG_KEY => {
                    self.tab();
                    println!("key:");

                    self.0 += 1;
                    let _ = walker.walk(DynVisitor(self)).await;
                    self.0 -= 1;

                    Flow::Continue.into()
                }
                crate::TAG_VALUE => {
                    self.tab();
                    println!("value:");

                    self.0 += 1;
                    let _ = walker.walk(DynVisitor(self)).await;
                    self.0 -= 1;

                    Flow::Continue.into()
                }
                _ => VisitResult::Skipped(walker),
            }
        })
    }
}

impl<'ctx, E: Effect> Value<'ctx, OwnedStatic<&'static str>, E> for Visitor<E> {
    fn visit<'a>(
        &'a mut self,
        OwnedStatic(value): OwnedStatic<&'static str>,
    ) -> Future<'a, VisitResult<OwnedStatic<&'static str>>, E>
    where
        'ctx: 'a,
    {
        self.tab();
        println!("{:?}", value);
        E::ready(Flow::Continue.into())
    }
}

impl<'ctx, E: Effect> Value<'ctx, OwnedStatic<usize>, E> for Visitor<E> {
    fn visit<'a>(
        &'a mut self,
        OwnedStatic(value): OwnedStatic<usize>,
    ) -> Future<'a, VisitResult<OwnedStatic<usize>>, E>
    where
        'ctx: 'a,
    {
        self.tab();
        println!("{}", value);
        E::ready(Flow::Continue.into())
    }
}

impl<'ctx, E: Effect> Value<'ctx, OwnedStatic<bool>, E> for Visitor<E> {
    fn visit<'a>(
        &'a mut self,
        OwnedStatic(value): OwnedStatic<bool>,
    ) -> Future<'a, VisitResult<OwnedStatic<bool>>, E>
    where
        'ctx: 'a,
    {
        self.tab();
        println!("{}", value);
        E::ready(Flow::Continue.into())
    }
}

impl<'ctx, E: Effect> Value<'ctx, OwnedStatic<&'static [&'static str]>, E> for Visitor<E> {
    fn visit<'a>(
        &'a mut self,
        OwnedStatic(value): OwnedStatic<&'static [&'static str]>,
    ) -> Future<'a, VisitResult<OwnedStatic<&'static [&'static str]>>, E>
    where
        'ctx: 'a,
    {
        self.tab();
        println!("{:?}", value);
        E::ready(Flow::Continue.into())
    }
}

impl<'ctx, E: Effect> Value<'ctx, OwnedStatic<TypeId>, E> for Visitor<E> {
    fn visit<'a>(
        &'a mut self,
        OwnedStatic(value): OwnedStatic<TypeId>,
    ) -> Future<'a, VisitResult<OwnedStatic<TypeId>>, E>
    where
        'ctx: 'a,
    {
        self.tab();
        println!("Visit type ID: {:?}", value);
        E::ready(Flow::Continue.into())
    }
}

impl<'ctx, E: Effect> Sequence<'ctx, E> for Visitor<E> {
    fn visit<'a>(
        &'a mut self,
        scope: protocol::visitor::DynSequenceScope<'a, 'ctx, E>,
    ) -> Future<'a, VisitResult<DynSequenceScope<'a, 'ctx, E>>, E> {
        E::wrap(async {
            self.tab();
            println!("sequence{:?}", scope.size_hint().await);

            self.0 += 1;

            let mut index = 0;
            let flow = loop {
                self.tab();
                println!("{}:", index);

                self.0 += 1;
                match scope.next(DynVisitor(self)).await {
                    Flow::Done => {
                        self.tab();
                        println!("<end>");
                        break Flow::Continue;
                    }
                    Flow::Continue => {}
                    Flow::Err => break Flow::Err,
                }
                index += 1;
                self.0 -= 1;
            };
            self.0 -= 2;
            flow.into()
        })
    }
}
