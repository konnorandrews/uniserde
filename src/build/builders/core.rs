// pub mod tracer;

use core::marker::PhantomData;

use effectful::{
    bound::{Bool, HasSend, IsSend},
    environment::{Environment, Thunk},
    thunk::{
        Ctx, CtxMut, IntoThunk as _, IntoThunkMaybeSend as _, ThunkMaybeSendOps as _, ThunkOps as _,
    },
};
use supply::Provider;
use ty_tag::lifetime_list::{L0, L2};

use crate::{
    protocol::{DynVisitor, VisitResult},
    walk::DynWalkerObjSafe,
};

// pub mod array;
// pub mod bool;
pub mod value;
// pub mod option;

// pub mod variant;
// pub mod r#enum;
// pub mod r#struct;
// pub mod tag_name;

#[derive(Default)]
#[non_exhaustive]
pub struct NoopVisitor<'src>(PhantomData<&'src ()>);

#[allow(unsafe_code)]
unsafe impl<'src, F: Bool> IsSend<F> for NoopVisitor<'src> {}

impl NoopVisitor<'_> {
    pub fn new() -> Self {
        Self(PhantomData)
    }
}

impl<'r, 'src> Provider<'r> for NoopVisitor<'src> {
    type Lifetimes = L2<'r, 'src>;
}

impl<'src> NoopVisitor<'src> {
    pub fn walk_dyn<'r, E: Environment>(
        walker: DynWalkerObjSafe<'r, 'src, E>,
    ) -> Thunk<'r, E, VisitResult> {
        Ctx::state((walker, NoopVisitor::new()))
            .into_thunk_maybe_send()
            .update_maybe_send(
                |CtxMut {
                     state: (walker, noop),
                     ..
                 }| {
                    walker
                        .walk(DynVisitor::new(noop))
                        .map(|x| x.output.to_continue().into())
                        .into_thunk_maybe_send()
                },
            )
            .no_state_maybe_send()
            .into_thunk()
    }
}
