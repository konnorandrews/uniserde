use super::value::{Cloneable, ValueBuilder};
use crate::Builder;
use effectful::environment::Environment;

macro_rules! value_builder {
    [$($ty:ty),*] => {
        $(impl<'src, M, E: Environment> crate::Build<'src, M, E> for $ty
        where
            ValueBuilder<$ty, Cloneable, E>: Builder<'src, E, Value = Self>,
        {
            type Builder = ValueBuilder<$ty, Cloneable, E>;
        })*
    };
}

value_builder![u8, u16, u32, u64, u128, usize];
value_builder![i8, i16, i32, i64, i128, isize];
value_builder![f32, f64];
value_builder![char];
value_builder![bool];
value_builder![()];
value_builder![String];
