use core::{fmt::Display, marker::PhantomData};

use effectful::{
    bound::{Bool, HasSend, IsSend},
    environment::{Environment, NeedSendOf, ThunkMaybeSend},
    thunk::{Ctx, IntoThunkMaybeSend as _},
};
use supply::{provide::ProviderDyn, Provider};
use ty_tag::{
    lifetime_list::{L1, L2},
    modifiers::AddLt,
    ReifySelf,
};

use crate::{
    protocol::{
        visitor::{MutValueTag, Value, ValueSrcSend},
        AsVisitor, DynVisitor,
    },
    Flow,
};

#[non_exhaustive]
pub struct ValueError<T>(PhantomData<fn() -> T>);

#[allow(unsafe_code)]
unsafe impl<F: Bool, T> IsSend<F> for ValueError<T> {}

impl<T> ::core::fmt::Debug for ValueError<T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "missing value of type `{}`", core::any::type_name::<T>())
    }
}

impl<T> Display for ValueError<T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "missing value of type `{}`", core::any::type_name::<T>())
    }
}

/// Builder for `'static` values.
///
/// This builder only uses the [`Value`] protocol.
///
#[doc = crate::doc_macro::mermaid!("value.mmd", 100)]
///
/// After
pub struct ValueBuilder<'src, T, E> {
    value: Option<T>,
    _marker: PhantomData<fn() -> (&'src (), E)>,
}

#[allow(unsafe_code)]
unsafe impl<T: Send, E, F: Bool> IsSend<F> for ValueBuilder<'_, T, E> {}

impl<T, E: Environment> crate::build::BuilderTypes<NeedSendOf<E>> for ValueBuilder<'_, T, E>
where
    // T: IsSend<NeedSendOf<E>>,
    T: Send,
{
    type Error = ValueError<T>;

    type Output = HasSend<T>;

    type Value = T;

    type Seed = ();

    fn unwrap_output(output: Self::Output) -> Self::Value {
        output.0
    }
}

impl<'src, T, E: Environment> crate::build::Builder<'src, E> for ValueBuilder<'src, T, E>
where
    // T: IsSend<NeedSendOf<E>>,
    T: Send,
    T: ReifySelf<L1<'src>>,
    // Self: for<'r> ProviderDyn<'r, L2<'r, 'src>>,
{
    fn build<'a>(self) -> ThunkMaybeSend<'a, E, Result<Self::Output, Self::Error>>
    where
        Self: 'a,
    {
        Ctx::output(
            self.value
                .map(HasSend)
                .ok_or(ValueError(Default::default())),
        )
        .into_thunk_maybe_send()
    }

    fn from_seed<'a>(_seed: Self::Seed) -> ThunkMaybeSend<'a, E, Self>
    where
        Self: 'a,
    {
        Ctx::output(Self {
            value: None,
            _marker: Default::default(),
        })
        .into_thunk_maybe_send()
    }
}

impl<'src, T, E: Environment> AsVisitor<'src, E> for ValueBuilder<'src, T, E>
where
    T: Send,
    T: ReifySelf<L1<'src>>,
    // Self: AnyTrait<'src> + DynBind<E>,
{
    fn as_visitor(&mut self) -> DynVisitor<'_, 'src, E> {
        DynVisitor::new(self)
    }
}

impl<'r, 'src, T, E: Environment> Provider<'r> for ValueBuilder<'src, T, E>
where
    T: Send,
    T: ReifySelf<L1<'src>>,
    // Self: RequestHint<'src, E> + Value<'src, OwnedStatic<T>, E>,
{
    type Lifetimes = L2<'r, 'src>;

    fn provide_mut(&'r mut self, want: &mut dyn supply::Want<Self::Lifetimes>) {
        want.provide_tag::<MutValueTag<ValueSrcSend<AddLt<T::Tag>>, E>>(self);
    }

    // fn upcast_by_id_mut(
    //     &mut self,
    //     id: crate::any::WithLtTypeId<'src>,
    // ) -> Option<crate::any::MutAnyUnsized<'_, 'src>> {
    //     trait_by_id!(&mut self, id, {
    //         type Impls = (dyn RequestHint<'src, E>, dyn Value<'src, OwnedStatic<T>, E>);
    //     });
    //
    //     None
    // }
}

// impl<'src, T: 'static + Clone, E: Environment> AnyTrait<'src> for ValueBuilder<T, Cloneable, E>
// where
//     T: DynBind<E> + IsSync<E::NeedSend>,
//     Self: Value<'src, OwnedStatic<T>, E>
//         + Value<'src, Raised<'src, 'src, BorrowedStatic<'src, i32>>, E>,
// {
//     fn upcast_by_id_mut(
//         &mut self,
//         id: crate::any::WithLtTypeId<'src>,
//     ) -> Option<crate::any::MutAnyUnsized<'_, 'src>> {
//         trait_by_id!(&mut self, id, {
//             type Impls = (
//                 dyn RequestHint<'src, E>,
//                 dyn Value<'src, OwnedStatic<T>, E>,
//                 dyn Value<'_, Raised<'_, '_, BorrowedStatic<'_, T>>, E>,
//                 dyn Value<'_, Raised<'_, '_, TempBorrowedStatic<'_, T>>, E>,
//             );
//         });
//
//         None
//     }
// }

// impl<'src, T: 'static, E: Environment> RequestHint<'src, E> for ValueBuilder<T, NotCloneable, E>
// where
//     T: DynBind<E>,
//     T: IsSync<E::NeedSend>,
// {
//     fn request_hint<'r>(
//         &'r mut self,
//         walker: DynWalker<'r, 'src, E>,
//     ) -> Canonical<'r, VisitResult, E> {
//         E::value((self, walker))
//             .update_map((), |_, (this, walker)| {
//                 hint_protocol::<dyn Value<'_, OwnedStatic<T>, E>, _, _>(walker.cast(), *this, ())
//                     .cast()
//             })
//             .map((), |_, (_, x)| x)
//             .cast()
//     }
// }

// impl<'src, T: 'static, E: Environment> RequestHint<'src, E> for ValueBuilder<T, Cloneable, E>
// where
//     T: Clone + DynBind<E> + IsSync<E::NeedSend>,
//     Self: AnyTrait<'src>,
// {
//     fn request_hint<'r>(
//         &'r mut self,
//         walker: DynWalker<'r, 'src, E>,
//     ) -> Canonical<'r, VisitResult, E> {
//         E::value((self, walker))
//             .update_map((), |_, (this, walker)| {
//                 hint_protocol::<dyn Value<'_, OwnedStatic<T>, E>, _, _>(walker.cast(), *this, ())
//                     .cast()
//             })
//             .cast::<()>()
//             .if_not_finished((), |_, (this, walker)| {
//                 hint_protocol::<
//                     dyn Value<'_, type_name::Raised<'_, '_, BorrowedStatic<'_, T>>, E>,
//                     _,
//                     _,
//                 >(walker.cast(), *this, ())
//                 .cast()
//             })
//             .cast::<()>()
//             .if_not_finished((), |_, (this, walker)| {
//                 hint_protocol::<
//                     dyn Value<'_, type_name::Raised<'_, '_, TempBorrowedStatic<'_, T>>, E>,
//                     _,
//                     _,
//                 >(walker.cast(), *this, ())
//                 .cast()
//             })
//             .cast::<()>()
//             .if_not_finished((), |_, (this, walker)| {
//                 hint_protocol::<
//                     dyn Value<'_, type_name::Raised<'_, '_, BorrowedMutStatic<'_, T>>, E>,
//                     _,
//                     _,
//                 >(walker.cast(), *this, ())
//                 .cast()
//             })
//             .cast::<()>()
//             .if_not_finished((), |_, (this, walker)| {
//                 hint_protocol::<
//                     dyn Value<'_, type_name::Raised<'_, '_, TempBorrowedMutStatic<'_, T>>, E>,
//                     _,
//                     _,
//                 >(walker.cast(), *this, ())
//                 .cast()
//             })
//             .map((), |_, (_, x)| x)
//             .cast()
//     }
// }

impl<'r, 'src, T, E: Environment> Value<'r, HasSend<T>, E> for ValueBuilder<'src, T, E>
where
    // T: IsSend<NeedSendOf<E>>,
    T: Send,
    // T: ReifySelf<L2<'r, 'src>>,
    // for<'a> AddLt<T::UsedTag>: ty_tag::ReifySized<L2<'a, 'src>, Reified = T>
{
    fn visit(
        &'r mut self,
        value: HasSend<T>,
    ) -> ThunkMaybeSend<'r, E, crate::protocol::VisitResult<HasSend<T>>>
// where
    //     ValueSrcSend<T::Tag>: ty_tag::ReifySized<L2<'r, 'src>, Reified: IsSend<NeedSendOf<E>>>
    {
        self.value = Some(value.0);

        Ctx::output(Flow::Done.into()).into_thunk_maybe_send()
    }

    // fn visit<'r>(
    //     &'r mut self,
    //     value: type_name::Lowered<'r, 'src, OwnedStatic<T>>,
    // ) -> Canonical<'r, VisitResult<type_name::Lowered<'r, 'src, OwnedStatic<T>>>, E>
    // where
    //     type_name::Lowered<'r, 'src, OwnedStatic<T>>: Sized + DynBind<E>,
    // {
    //     self.value = Some(value.0);
    //
    //     E::value(Flow::Done.into()).cast()
    // }
}

// impl<'src, T: 'static, E: Environment>
//     Value<'src, type_name::Raised<'src, 'src, BorrowedStatic<'src, T>>, E>
//     for ValueBuilder<T, Cloneable, E>
// where
//     T: DynBind<E> + Clone,
// {
//     fn visit<'r>(
//         &'r mut self,
//         value: type_name::Lowered<'r, 'src, type_name::Raised<'r, 'src, BorrowedStatic<'src, T>>>,
//     ) -> Canonical<
//         'r,
//         VisitResult<
//             type_name::Lowered<'r, 'src, type_name::Raised<'r, 'src, BorrowedStatic<'src, T>>>,
//         >,
//         E,
//     >
//     where
//         type_name::Lowered<'r, 'src, type_name::Raised<'r, 'src, BorrowedStatic<'src, T>>>:
//             Sized + DynBind<E>,
//     {
//         self.value = Some(value.0.clone());
//
//         E::value(Flow::Done.into()).cast()
//     }
// }

// impl<'ctx, T: 'static, E: Environment>
//     Value<'ctx, type_name::Raised<'ctx, 'ctx, TempBorrowedStatic<'ctx, T>>, E>
//     for ValueBuilder<T, Cloneable, E>
// where
//     T: Clone + DynBind<E>,
// {
//     fn visit<'a>(
//         &'a mut self,
//         TempBorrowedStatic(value): TempBorrowedStatic<'a, T>,
//     ) -> Canonical<'a, VisitResult<TempBorrowedStatic<'a, T>>, E>
//     where
//         'ctx: 'a,
//         TempBorrowedStatic<'a, T>: DynBind<E>,
//     {
//         self.value = Some(value.clone());
//
//         E::value(Flow::Done.into()).cast()
//     }
// }

// impl<'ctx, T: 'static, E: Environment> Value<'ctx, BorrowedMutStatic<'ctx, T>, E>
//     for ValueBuilder<T, Cloneable, E>
// where
//     T: Clone,
//     Dynamic<T>: DynBind<E>,
//     Dynamic<BorrowedMutStatic<'ctx, T>>: DynBind<E>,
// {
//     fn visit<'a>(
//         &'a mut self,
//         BorrowedMutStatic(value): BorrowedMutStatic<'ctx, T>,
//     ) -> Canonical<'a, VisitResult<Dynamic<BorrowedMutStatic<'ctx, T>>>, E>
//     where
//         'ctx: 'a,
//     {
//         self.value = Some(Dynamic(value.clone()));
//
//         E::value(Flow::Done.into()).cast()
//     }
// }
//
// impl<'ctx, T: 'static, E: Environment>
//     Value<'ctx, type_name::Raised<'static, 'ctx, BorrowedMutStatic<'static, T>>, E>
//     for ValueBuilder<T, Cloneable, E>
// where
//     T: Clone,
//     Dynamic<T>: DynBind<E>,
//     for<'a> Dynamic<TempBorrowedMutStatic<'a, T>>: DynBind<E>,
// {
//     fn visit<'a>(
//         &'a mut self,
//         TempBorrowedMutStatic(value): TempBorrowedMutStatic<'a, T>,
//     ) -> Canonical<'a, VisitResult<Dynamic<TempBorrowedMutStatic<'a, T>>>, E>
//     where
//         'ctx: 'a,
//     {
//         self.value = Some(Dynamic(value.clone()));
//
//         E::value(Flow::Done.into()).cast()
//     }
// }
