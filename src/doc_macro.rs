macro_rules! mermaid {
    ($file:literal, $height:literal) => {
        concat!(
            "<pre class=\"mermaid\" style=\"padding:0;max-height:90vh;height:",
            stringify!($height),
            "vw;\">\n",
            "<div style=\"visibility:hidden;\">\n",
            include_str!($file),
            "\n<div>\n",
            "</pre>\n\n",
            "<script type=\"module\">\n",
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/src/doc_mermaid_injector.js"
            )),
            "</script>\n\n"
        )
    };
}
pub(crate) use mermaid;
