//! A collection of built in protocols between walkers and visitors.
//!
//! Treaty has not set data model. Instead, this module contains a set of basic protocols
//! walkers and visitors can use to exchange information.
//!
//! | Rust Type (`T`) | Protocol |
//! |-----------|----------|
//! | `bool`    | [`dyn Value<'_, OwnedStatic<T>, _>`][visitor::Value] |
//! | `i8`, `i16`, `i32`, `i64`, `i128`, `isize`    | [`dyn Value<'_, OwnedStatic<T>, _>`][visitor::Value] |
//! | `u8`, `u16`, `u32`, `u64`, `u128`, `usize`    | [`dyn Value<'_, OwnedStatic<T>, _>`][visitor::Value] |
//! | `f32`, `f64` | [`dyn Value<'_, OwnedStatic<T>, _>`][visitor::Value] |
//! | `char` | [`dyn Value<'_, OwnedStatic<T>, _>`][visitor::Value] |
//! | `String` | [`dyn Value<'_, OwnedStatic<T>, _>`][visitor::Value] |
//! | `Vec<u8>` | [`dyn Value<'_, OwnedStatic<T>, _>`][visitor::Value] |
//! | `&'src str` | [`dyn Value<'ctx, BorrowedStatic<'ctx, T>, _>`][visitor::Value] |
//! | `&'ctx [u8]` | [`dyn Value<'ctx, BorrowedStatic<'ctx, T>, _>`][visitor::Value] |
//!
//!
//!
//! Interface for interfaces.
//!
//! ## Design
//! The design of protocols is based on an idea found in the
//! [`gdbstub`](https://docs.rs/gdbstub/latest/gdbstub/target/ext/index.html) crate.
//! This idea is of so called inlinable dyn extension traits.
//! However, in the form given in `gdbstub` they can't be used for arbitrary interfaces.
//! The main trait still needs to know about all the possible protocols.
//! That is where this module comes in.
//!
//! This module implements a technique we name dynamic inlinable dyn extension traits (DIDETs).
//! DIDETs adds one more layer to IDETs. Instead of a trait that knows all the possible protocols,
//! we have a single trait [`Implementer`] that allows looking up an extension trait
//! using a type ID. This may seem like it defeats the purpose of IDETs, that being to
//! make them inlinable. However, it turns out LLVM (the optimizer) is able to see
//! through this style of runtime reflection. As such, we still gain the benefits of
//! IDETs but with more flexability.
//! Protocols can now be defined in *any* crate and used between arbitrary crates.
//!
//! A protocol is a special trait that can participate as a DIDET. The only thing needed
//! for a protocol is an associated trait object. Because we need to use the
//! [`TypeId`][core::any::TypeId] of a protocol to perform reflection, we can't just use
//! the trait object itself as the protocol type. Instead an uninhabited type is used
//! as a marker for the trait.
//!
//! We then "implement" a protocol for a type by using [`Implementation`]. This provides
//! a mapping from `T` to the protocol's trait object.
//! By itself, [`Implementation`] is not enough for DIDET. A type also needs to implement
//! [`Implementer`] which allows looking up a particular [`Implementation`] trait object
//! from a [`ProtocolId`].
//!
//! The implementation of DIDETs defined by this module allows [`Implementer`] to be object safe.
//! This is done via the help of the [`AnyImpl`] type. This is not required for the core
//! idea of DIDETs.

pub mod visitor;
pub mod walker;

use core::ops::{Deref, DerefMut};

use effectful::{
    bound::{Bool, IsSend},
    environment::{Environment, NeedSendOf},
};
use supply::provide::ProviderDyn;
use ty_tag::lifetime_list::{L1, L2};

use crate::{Flow, Status};

pub trait AnyTraitDyn<'lt, E: Environment>:
    for<'r> ProviderDyn<'r, L2<'r, 'lt>> + IsSend<NeedSendOf<E>>
{
}

impl<'lt, E, T> AnyTraitDyn<'lt, E> for T
where
    E: Environment,
    T: for<'r> ProviderDyn<'r, L2<'r, 'lt>> + IsSend<NeedSendOf<E>>,
{
}

pub struct DynVisitor<'r, 'src, E: Environment> {
    any: &'r mut dyn AnyTraitDyn<'src, E>,
}

const _: () = {
    const fn is_send<T: Send>() {}

    is_send::<&mut dyn Send>();
};

#[allow(unsafe_code)]
unsafe impl<'r, 'src, E: Environment> IsSend<NeedSendOf<E>> for DynVisitor<'r, 'src, E> {}

impl<'r, 'src, E: Environment> DynVisitor<'r, 'src, E> {
    pub fn new(any: &'r mut dyn AnyTraitDyn<'src, E>) -> Self {
        DynVisitor { any }
    }

    pub fn cast(&mut self) -> DynVisitor<'_, 'src, E> {
        DynVisitor::new(self.any)
    }

    pub fn into_inner(self) -> &'r mut dyn AnyTraitDyn<'src, E> {
        self.any
    }
}

impl<'r, 'src, E: Environment> Deref for DynVisitor<'r, 'src, E> {
    type Target = dyn AnyTraitDyn<'src, E> + 'r;

    fn deref(&self) -> &Self::Target {
        self.any
    }
}

impl<E: Environment> DerefMut for DynVisitor<'_, '_, E> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.any
    }
}

pub trait AsVisitor<'src, E: Environment> {
    fn as_visitor(&mut self) -> DynVisitor<'_, 'src, E>;
}

impl<'src, E: Environment> AsVisitor<'src, E> for DynVisitor<'_, 'src, E> {
    fn as_visitor(&mut self) -> DynVisitor<'_, 'src, E> {
        DynVisitor::new(self.any)
    }
}

pub struct DynWalker<'r, 'src, E: Environment> {
    any: &'r mut dyn AnyTraitDyn<'src, E>,
}

#[allow(unsafe_code)]
unsafe impl<E: Environment> IsSend<NeedSendOf<E>> for DynWalker<'_, '_, E> {}

impl<'r, 'src, E: Environment> DynWalker<'r, 'src, E> {
    pub fn new(any: &'r mut dyn AnyTraitDyn<'src, E>) -> Self {
        DynWalker { any }
    }

    pub fn cast(&mut self) -> DynWalker<'_, 'src, E> {
        DynWalker::new(self.any)
    }

    pub fn into_inner(self) -> &'r mut dyn AnyTraitDyn<'src, E> {
        self.any
    }
}

impl<'r, 'src, E: Environment> Deref for DynWalker<'r, 'src, E> {
    type Target = dyn AnyTraitDyn<'src, E> + 'r;

    fn deref(&self) -> &Self::Target {
        self.any
    }
}

impl<E: Environment> DerefMut for DynWalker<'_, '_, E> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.any
    }
}

#[derive(Copy, Clone)]
#[must_use]
pub enum VisitResult<S = ()> {
    /// The protocol was not used.
    ///
    /// This either means the visitor doesn't support the protocol at all, or
    /// it didn't want to use the protocol right now.
    Skipped(S),

    /// How control flow should proceed.
    Control(Flow),
}

const _: () = {
    const fn is_send<T: Send>() {}

    is_send::<Flow>();
};

#[allow(unsafe_code)]
unsafe impl<S: IsSend<F>, F: Bool> IsSend<F> for VisitResult<S> {}

impl<S> VisitResult<S> {
    pub fn unit_skipped(self) -> VisitResult<()> {
        match self {
            VisitResult::Skipped(_) => VisitResult::Skipped(()),
            VisitResult::Control(flow) => VisitResult::Control(flow),
        }
    }

    pub fn to_status(self) -> Status {
        match self {
            VisitResult::Skipped(_) => Status::Ok,
            VisitResult::Control(flow) => flow.to_status(),
        }
    }

    pub fn to_flow(self) -> Option<Flow> {
        match self {
            VisitResult::Skipped(_) => None,
            VisitResult::Control(flow) => Some(flow),
        }
    }

    pub fn dont_break_if_skipped(self) -> Option<Status> {
        match self {
            VisitResult::Skipped(_) => None,
            VisitResult::Control(Flow::Continue) => Some(Status::Ok),
            VisitResult::Control(Flow::Done) => Some(Status::Ok),
            VisitResult::Control(Flow::Err) => Some(Status::Err),
        }
    }

    pub fn break_if_done_or_err(self) -> Option<Status> {
        match self {
            VisitResult::Skipped(_) => None,
            VisitResult::Control(Flow::Continue) => None,
            VisitResult::Control(Flow::Done) => Some(Status::Ok),
            VisitResult::Control(Flow::Err) => Some(Status::Err),
        }
    }

    pub fn break_if_err(self) -> Option<Status> {
        match self {
            VisitResult::Skipped(_) => None,
            VisitResult::Control(Flow::Continue) => None,
            VisitResult::Control(Flow::Done) => None,
            VisitResult::Control(Flow::Err) => Some(Status::Err),
        }
    }

    pub fn map_skipped<R, F>(self, f: F) -> VisitResult<R>
    where
        F: FnOnce(S) -> R,
    {
        match self {
            VisitResult::Skipped(s) => VisitResult::Skipped(f(s)),
            VisitResult::Control(flow) => VisitResult::Control(flow),
        }
    }
}

impl<S> PartialEq for VisitResult<S> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Skipped(_), Self::Skipped(_)) => true,
            (Self::Control(l0), Self::Control(r0)) => l0 == r0,
            _ => false,
        }
    }
}

impl<S> core::fmt::Debug for VisitResult<S> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::Skipped(_) => f.debug_tuple("Skipped").finish(),
            Self::Control(arg0) => f.debug_tuple("Control").field(arg0).finish(),
        }
    }
}

impl<S> From<Flow> for VisitResult<S> {
    fn from(value: Flow) -> Self {
        Self::Control(value)
    }
}
