use core::ops::ControlFlow;

use crate::{Flow, Status};

mod recoverable;
mod request_hint;
mod sequence;
// mod tag;
mod value;

pub use recoverable::*;
pub use request_hint::*;
pub use sequence::*;
// pub use tag::*;
pub use value::*;

// pub type IfSkippedEffective<'ctx, 'lt, 'wrap, Cap, Update, Eff> = UpdatePartial<
//     'wrap,
//     'lt,
//     (),
//     (
//         Cap,
//         HasSendAndSync<
//             for<'a> fn(
//                 Cap,
//                 &'a mut Update,
//             )
//                 -> Canonical<'a, VisitResult<()>, <Eff as InEnvironment>::Env, &'ctx ()>,
//         >,
//     ),
//     (),
//     Update,
//     (),
//     VisitResult<()>,
//     (Update, VisitResult<()>),
//     Eff,
// >;
//
// type IfSkippedF<'ctx, Cap, Update, Eff> =
//     for<'a> fn(
//         Cap,
//         &'a mut Update,
//     ) -> Canonical<'a, VisitResult<()>, <Eff as InEnvironment>::Env, &'ctx ()>;
//
// pub trait EffectiveVisitExt<'lt>: Effective<'lt> {
//     fn if_skipped<'ctx, 'wrap, Cap, Update>(
//         self,
//         cap: Cap,
//         f: IfSkippedF<'ctx, Cap, Update, Self>,
//     ) -> IfSkippedEffective<'ctx, 'lt, 'wrap, Cap, Update, Self>
//     where
//         Cap: DynBind<Self::Env>,
//         Update: DynBind<Self::Env>,
//         Self: Effective<'lt, Output = (Update, VisitResult<()>)>,
//         'ctx: 'lt,
//         'lt: 'wrap,
//     {
//         self.update_partial(
//             (),
//             |_, (update, result)| match result {
//                 VisitResult::Skipped(()) => (update, ControlFlow::Continue(())),
//                 VisitResult::Control(_) => (update, ControlFlow::Break(result)),
//             },
//             (cap, HasSendAndSync(f)),
//             |(cap, HasSendAndSync(f)), update, ()| f(cap, update).cast().into_raw(),
//             (),
//             |_, update, out| (update, out),
//         )
//     }
//
//     fn if_not_finished<'ctx, 'wrap, Cap, Update>(
//         self,
//         cap: Cap,
//         f: IfSkippedF<'ctx, Cap, Update, Self>,
//     ) -> IfSkippedEffective<'ctx, 'lt, 'wrap, Cap, Update, Self>
//     where
//         Cap: DynBind<Self::Env>,
//         Update: DynBind<Self::Env>,
//         Self: Effective<'lt, Output = (Update, VisitResult<()>)>,
//         'ctx: 'lt,
//         'lt: 'wrap,
//     {
//         self.update_partial(
//             (),
//             |_, (update, result)| match result {
//                 VisitResult::Skipped(()) | VisitResult::Control(Flow::Continue) => {
//                     (update, ControlFlow::Continue(()))
//                 }
//                 VisitResult::Control(_) => (update, ControlFlow::Break(result)),
//             },
//             (cap, HasSendAndSync(f)),
//             |(cap, HasSendAndSync(f)), update, ()| f(cap, update).cast().into_raw(),
//             (),
//             |_, update, out| (update, out),
//         )
//     }
// }
//
// impl<'lt, T: Effective<'lt>> EffectiveVisitExt<'lt> for T {}
