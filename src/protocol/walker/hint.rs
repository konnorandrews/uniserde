//! Protocol for giving a hint to a walker.
//!
//! Sometimes a walker has multiple protocols it could use,
//! this module gives a protocol by which a visitor can give a hint
//! to the walker about what it is expecting.

use core::{
    marker::PhantomData,
    ops::{Deref, DerefMut},
};

use effectful::{
    bound::IsSend,
    environment::{EnvOf, InEnvironment, NeedSendOf, Thunk, ThunkMaybeSend},
    thunk::{Ctx, IntoThunk},
};
use supply::ProviderExt;
use ty_tag::{
    external_tags, lifetime_list::L2, modifiers::RepeatLt, tag, ReifiedOf, ReifySized, Tagged,
};

use crate::protocol::{DynVisitor, DynWalker, VisitResult};

/// Meta information for the hint.
///
/// This gives the visitor more information to work from when selecting a hint.
pub trait HintMeta<'r, 'src: 'r, Bound = &'r &'src ()>: InEnvironment + Sized + 'static {
    /// Information known by the walker.
    ///
    /// This should be information easy to get without changing the state of the walker
    /// in an irreversible way.
    type Known: IsSend<NeedSendOf<Self::Env>>;

    /// Extra information the visitor can give to the walker about what it is expecting.
    type Hint: IsSend<NeedSendOf<Self::Env>>;
}

pub type HintOf<'r, 'src, T> = <T as HintMeta<'r, 'src>>::Hint;
pub type KnownOf<'r, 'src, T> = <T as HintMeta<'r, 'src>>::Known;

/// Object implementing the [`Hint`] protocol.
pub trait Hint<'src, ProtocolTag: for<'r> HintMeta<'r, 'src>>:
    IsSend<NeedSendOf<ProtocolTag::Env>>
{
    /// Hint to the walker to use the `P` protocol.
    ///
    /// This should only be called once per [`RequestHint`].
    fn hint<'r>(
        &'r mut self,
        visitor: DynVisitorWith<'r, 'src, ProtocolTag>,
        hint: HintOf<'r, 'src, ProtocolTag>,
    ) -> Thunk<'r, ProtocolTag::Env, VisitResult>;

    /// Ask the walker for information about it's support of the protocol.
    fn known<'r>(
        &'r mut self,
        hint: &'r HintOf<'r, 'src, ProtocolTag>,
    ) -> ThunkMaybeSend<'r, ProtocolTag::Env, Result<KnownOf<'r, 'src, ProtocolTag>, ()>>;
}

#[tag]
pub type HintTag<'u, 'src, #[itself] ProtocolTag: 'static> = dyn Hint<'src, ProtocolTag> + 'u;

pub type MutHintTag<ProtocolTag> = RepeatLt<external_tags::core::RefMut<HintTag<ProtocolTag>>>;

pub struct DynVisitorWith<'r, 'src, ProtocolTag: HintMeta<'r, 'src>> {
    visitor: DynVisitor<'r, 'src, ProtocolTag::Env>,
    _marker: PhantomData<ProtocolTag>,
}

impl<'r, 'src, ProtocolTag: HintMeta<'r, 'src>> DynVisitorWith<'r, 'src, ProtocolTag> {
    pub fn new(visitor: DynVisitor<'r, 'src, ProtocolTag::Env>) -> Self {
        Self {
            visitor,
            _marker: PhantomData,
        }
    }

    pub fn into_inner(self) -> DynVisitor<'r, 'src, ProtocolTag::Env> {
        self.visitor
    }
}

impl<'r, 'src, ProtocolTag: HintMeta<'r, 'src>> Deref for DynVisitorWith<'r, 'src, ProtocolTag> {
    type Target = DynVisitor<'r, 'src, ProtocolTag::Env>;

    fn deref(&self) -> &Self::Target {
        &self.visitor
    }
}

impl<'r, 'src, ProtocolTag: HintMeta<'r, 'src>> DerefMut for DynVisitorWith<'r, 'src, ProtocolTag> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.visitor
    }
}

pub fn hint_protocol<'r, 'src, Protocol>(
    walker: DynWalker<'r, 'src, EnvOf<Protocol::Tag>>,
    visitor: DynVisitor<'r, 'src, EnvOf<Protocol::Tag>>,
    hint: HintOf<'r, 'src, Protocol::Tag>,
) -> Thunk<'r, EnvOf<Protocol::Tag>, VisitResult>
where
    Protocol: ?Sized + Tagged<Tag: for<'a> HintMeta<'a, 'src>>,
{
    if let Some(object) = walker
        .into_inner()
        .request_mut::<MutHintTag<Protocol::Tag>>()
    {
        object.hint(DynVisitorWith::new(visitor), hint)
    } else {
        Ctx::output(VisitResult::Skipped(())).into_thunk()
    }
}
