use effectful::{
    bound::IsSend,
    environment::{Environment, NeedSendOf, Thunk, ThunkMaybeSend},
    thunk::{Ctx, CtxMut, IntoThunkMaybeSend as _, ThunkMaybeSendOps as _, ThunkOps as _},
};
use ty_tag::{external_tags, modifiers::RepeatLt, tag};

use crate::protocol::{DynVisitor, DynWalker, VisitResult};
use supply::ProviderExt;

/// Protocol for requesting a hint from a visitor.
pub trait RequestHint<'src, E: Environment>: IsSend<NeedSendOf<E>> {
    /// Call this to request a hint.
    ///
    /// `walker` is what the visitor (`self`) will call to give a hint using the
    /// [`Hint`][crate::builtins::walker::Hint] protocol.
    fn request_hint<'r>(&'r mut self, walker: DynWalker<'r, 'src, E>) -> Thunk<'r, E, VisitResult>;
}

#[tag]
pub type RequestHintTag<'u, 'src, #[itself] E: Environment> = dyn RequestHint<'src, E> + 'u;

pub type MutRequestHintTag<E> = RepeatLt<external_tags::core::RefMut<RequestHintTag<E>>>;

/// Visit using the [`RequestHint`] protocol.
///
/// If [`Flow::Continue`] is returned then the visitor wants/needs more information it didn't get
/// from the hints.
/// If [`Flow::Done`] is returned then the visitor doesn't need any more information and the walker
/// should stop walking.
/// If [`Flow::Break`] is returned then there was an error and the walker should stop walking.
pub fn request_hint<'r, 'src, E: Environment>(
    visitor: DynVisitor<'r, 'src, E>,
    walker: DynWalker<'r, 'src, E>,
) -> ThunkMaybeSend<'r, E, VisitResult> {
    Ctx::state((visitor, walker))
        .into_thunk_maybe_send()
        .update_maybe_send(
            |CtxMut {
                 state: (visitor, walker),
                 ..
             }| {
                if let Some(object) = visitor.request_mut::<MutRequestHintTag<E>>() {
                    object.request_hint(walker.cast()).into_thunk_maybe_send()
                } else {
                    Ctx::output(VisitResult::Skipped(())).into_thunk_maybe_send()
                }
            },
        )
        .no_state_maybe_send()
}
