//! [`Protocol`] for giving a visitor an owned value.
//!
//! In some sense, this is the most basic protocol.

use effectful::{
    bound::{Bool, HasSend, IsSend},
    environment::{Environment, NeedSendOf, ThunkMaybeSend},
    thunk::{Ctx, IntoThunkMaybeSend as _},
};
use supply::ProviderExt as _;
use ty_tag::{
    external_tags, lifetime_list::L2, modifiers::RepeatLt, tag, ReifiedOf, Reify, ReifySized, Tag,
    Tagged,
};

use crate::protocol::{walker::hint::HintMeta, DynVisitor, VisitResult};
use ty_tag::lifetime_list::{cons::Cons, L1};
use ty_tag::WithLt;

#[ty_tag::tag(remote)]
pub type ValueSrcSend<T: Send> = HasSend<T>;

/// Trait object for the [`Value`] protocol.
///
/// Types implementing the [`Value`] protocol will implement this trait.
pub trait Value<'r, T: IsSend<NeedSendOf<E>>, E: Environment> {
    /// Visit a value of type `T`.
    ///
    /// Use this to give a value to a visitor. Its expected that a walker
    /// only calls this once per usage of the trait object, but that is not
    /// forced.
    ///
    /// If a [`ControlFlow::Break`] is returned then the walker
    /// should stop walking as soon as possible as there has likely been
    /// and error.
    fn visit(&'r mut self, value: T) -> ThunkMaybeSend<'r, E, VisitResult<T>>;
}

// #[tag]
// pub type ValueTag<'u, T, #[itself] E: Environment> = dyn Value<T, E> + 'u;

// ///Tag for type `dyn Value<'src, T, E> + 'u`.
// pub struct ValueTag<T: ?Sized, E: ?Sized>(
//     ::ty_tag::__,
//     ::core::marker::PhantomData<fn() -> (*const T, *const E)>,
// );

// impl<
//     'u,
//     __Group: ?Sized,
//     T: ::ty_tag::Tagged<__Group>,
//     E: Environment,
// > ::ty_tag::Tagged<__Group> for dyn Value<'u, T, E> + 'u {
//     type Tag = ValueTag<<T as ::ty_tag::Tagged<__Group>>::Tag, E>;
// }
//
// impl<
//     __Group: ?Sized,
//     T: ?Sized + ::ty_tag::Tagged<__Group>,
//     E: Environment,
// > ::ty_tag::Tagged<__Group> for ValueTag<T, E> {
//     type Tag = ValueTag<<T as ::ty_tag::Tagged<__Group>>::Tag, E>;
// }
//
// impl<T: ?Sized + ::ty_tag::Tag, E: Environment> ::ty_tag::Tag for ValueTag<T, E> {
//     type NeededLifetimes = <T as Tag>::NeededLifetimes;
// }
//
// impl<
//     'u,
//     __L: ::ty_tag::lifetime_list::LifetimeList<Head = L1<'u>>,
//     T: ?Sized + ::ty_tag::WithLt<__L>,
//     E: Environment,
// > ::ty_tag::WithLt<
//     __L
// > for ValueTag<T, E>
// where
//     <T as ::ty_tag::WithLt<__L>>::Reified: Sized,
// {
//     type Reified = dyn Value<'u, <T as ::ty_tag::WithLt<__L>>::Reified, E> + 'u;
// }
//
// pub type MutValueTag<T, E> = RepeatLt<external_tags::core::RefMut<ValueTag<T, E>>>;

#[tag]
pub type KnownTag<'r, T: ?Sized + 'r> = ValueKnown<'r, T>;

// // This enrolls the Value protocol into the walker hint system.
// impl<'r, 'src, T: Tag + Reify<L2<'r, 'src>, Reified: IsSend<NeedSendOf<E>>>, E: Environment> HintMeta<'r, 'src> for ValueTag<T, E> {
//     type Known = ValueKnown<'r, ReifiedOf<L2<'r, 'src>, T>>;
//
//     type Hint = ();
// }
//
// impl<T: Tag + Tagged, E: Environment> effectful::environment::InEnvironment for ValueTag<T, E> {
//     type Env = E;
// }

#[derive(PartialEq, Debug)]
pub struct ValueKnown<'r, T: ?Sized> {
    /// A preview of the value.
    ///
    /// This can be used to inspect the value before committing to a visit.
    pub preview: Option<&'r mut T>,
}

const _: () = {
    fn is_send<T: Send>() {}

    fn with_t<T: Send>() {
        is_send::<ValueKnown<'_, T>>()
    }
};

#[allow(unsafe_code)]
unsafe impl<T: ?Sized + IsSend<F>, F: Bool> IsSend<F> for ValueKnown<'_, T> {}

#[tag(remote)]
pub type MutValueTag<'r, 'src, #[flatten] T: 'r, #[itself] E: Environment> =
    &'r mut dyn Value<'r, T, E>;

pub fn visit_value<'r, 'src, T, E>(
    visitor: DynVisitor<'r, 'src, E>,
    value: ReifiedOf<L2<'r, 'src>, T>,
) -> ThunkMaybeSend<'r, E, VisitResult<ReifiedOf<L2<'r, 'src>, T>>>
where
    E: Environment,
    T: Tag,
    T: ReifySized<L2<'r, 'src>, Reified: IsSend<NeedSendOf<E>>>,
{
    if let Some(object) = visitor.into_inner().request_mut::<MutValueTag<T::Tag, E>>() {
        // Allow the visitor to give a hint if it wants.
        object.visit(value)
    } else {
        // If the visitor doesn't support request hint then we continue.
        Ctx::output(VisitResult::Skipped(value)).into_thunk_maybe_send()
    }
}
