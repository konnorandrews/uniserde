use effectful::{
    bound::IsSend,
    environment::{Environment, InEnvironment, NeedSendOf, Thunk},
    thunk::{Ctx, IntoThunk as _},
};
use supply::ProviderExt as _;
use ty_tag::{external_tags, modifiers::RepeatLt, tag};

use crate::{
    protocol::{walker::hint::HintMeta, DynVisitor, VisitResult},
    Status,
};

pub trait Recoverable<'src, E: Environment>: IsSend<NeedSendOf<E>> {
    fn visit<'r>(
        &'r mut self,
        scope: DynRecoverableScope<'r, 'src, E>,
    ) -> Thunk<'r, E, VisitResult>;
}

#[tag]
pub type RecoverableTag<'u, 'src, #[itself] E: Environment> = dyn Recoverable<'src, E> + 'u;

pub type MutRecoverableTag<E> = RepeatLt<external_tags::core::RefMut<RecoverableTag<E>>>;

impl<E: Environment> HintMeta<'_, '_> for RecoverableTag<E> {
    type Known = ();

    type Hint = ();
}

impl<E: Environment> InEnvironment for RecoverableTag<E> {
    type Env = E;
}

pub trait RecoverableScope<'src, E: Environment>: IsSend<NeedSendOf<E>> {
    fn new_walk<'r>(&'r mut self, visitor: DynVisitor<'r, 'src, E>) -> Thunk<'r, E, Status>;
}

pub type DynRecoverableScope<'r, 'src, E> = &'r mut dyn RecoverableScope<'src, E>;

pub fn visit_recoverable<'r, 'src, E: Environment>(
    visitor: DynVisitor<'r, 'src, E>,
    scope: DynRecoverableScope<'r, 'src, E>,
) -> Thunk<'r, E, VisitResult> {
    if let Some(object) = visitor.into_inner().request_mut::<MutRecoverableTag<E>>() {
        // Allow the visitor to give a hint if it wants.
        object.visit(scope)
    } else {
        // If the visitor doesn't support request hint then we continue.
        Ctx::output(VisitResult::Skipped(())).into_thunk()
    }
}
