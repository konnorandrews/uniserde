use effectful::{
    bound::{HasSend, IsSend},
    environment::{Environment, InEnvironment, NeedSendOf, Thunk},
    thunk::{Ctx, IntoThunk as _},
};
use supply::ProviderExt as _;
use ty_tag::{external_tags, modifiers::RepeatLt, tag};

use crate::{
    protocol::{walker::hint::HintMeta, DynVisitor, VisitResult},
    Flow,
};

/// Protocol for visiting a sequence.
///
/// This protocol uses a scope to give temporary control to the visitor.
/// The visitor will drive the walker for each item.
pub trait Sequence<'src, E: Environment>: IsSend<NeedSendOf<E>> {
    fn visit<'r>(&'r mut self, scope: DynSequenceScope<'r, 'src, E>) -> Thunk<'r, E, VisitResult>;
}

#[tag]
pub type SequenceTag<'u, 'src, #[itself] E: Environment> = dyn Sequence<'src, E> + 'u;

pub type MutSequenceTag<E> = RepeatLt<external_tags::core::RefMut<SequenceTag<E>>>;

impl<E: Environment> HintMeta<'_, '_> for SequenceTag<E> {
    type Known = HasSend<SequenceKnown>;

    type Hint = HasSend<SequenceHint>;
}

impl<E: Environment> InEnvironment for SequenceTag<E> {
    type Env = E;
}

pub trait SequenceScope<'src, E: Environment>: IsSend<NeedSendOf<E>> {
    fn size_hint(&mut self) -> Thunk<'_, E, (usize, Option<usize>)>;

    fn next<'r>(&'r mut self, visitor: DynVisitor<'r, 'src, E>) -> Thunk<'r, E, Flow>;
}

pub type DynSequenceScope<'r, 'src, E> = &'r mut dyn SequenceScope<'src, E>;

#[derive(Default)]
pub struct SequenceKnown {
    pub len: (usize, Option<usize>),
}

pub struct SequenceHint {
    pub len: (usize, Option<usize>),
}

#[inline(always)]
pub fn visit_sequence<'r, 'src, E: Environment>(
    visitor: DynVisitor<'r, 'src, E>,
    scope: DynSequenceScope<'r, 'src, E>,
) -> Thunk<'r, E, VisitResult> {
    if let Some(object) = visitor.into_inner().request_mut::<MutSequenceTag<E>>() {
        // Allow the visitor to walk the sequence scope.
        object.visit(scope)
    } else {
        // If the visitor doesn't support sequence then we continue.
        Ctx::output(VisitResult::Skipped(())).into_thunk()
    }
}
