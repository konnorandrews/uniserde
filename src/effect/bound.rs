use crate::never::Never;

pub trait Bool: Sized + sealed_bool::Sealed + 'static {}

mod sealed_bool {
    use super::*;

    pub trait Sealed {}

    impl Sealed for Yes {}
    impl Sealed for No {}
}

pub enum Yes {}
pub enum No {}

impl Bool for Yes {}
impl Bool for No {}

pub unsafe trait IsSend<B: Bool> {}
pub unsafe trait IsSync<B: Bool> {}

pub struct DynamicShim<T>(pub T);

unsafe impl<T: IsSend<Yes>> Send for DynamicShim<T> {}
unsafe impl<T: IsSync<Yes>> Sync for DynamicShim<T> {}

pub struct Dynamic<T>(pub T);

unsafe impl<T> IsSend<No> for Dynamic<T> {}
unsafe impl<T: Send> IsSend<Yes> for Dynamic<T> {}
unsafe impl<T> IsSync<No> for Dynamic<T> {}
unsafe impl<T: Sync> IsSync<Yes> for Dynamic<T> {}

pub struct SendAndSync<T>(pub T);

unsafe impl<T: Send, F: Bool> IsSend<F> for SendAndSync<T> {}
unsafe impl<T: Sync, F: Bool> IsSync<F> for SendAndSync<T> {}

unsafe impl<F: Bool> IsSend<F> for Never {}
unsafe impl<F: Bool> IsSync<F> for Never {}

unsafe impl<F: Bool> IsSend<F> for () {}
unsafe impl<F: Bool> IsSync<F> for () {}
